## Ejercicio - Uniendo hashes

Usar dos métodos `join_hash` y `merge_hash` para regresar la unión de los hashes que se reciben como argumentos.


>Restricción: 
- No uses `merge`.
- Usar asignación dinámica de argumentos.
- Dividir responsabilidades ([Single Responsibility Principle](https://thoughtbot.com/blog/back-to-basics-solid)).


```ruby
#join_hash method



#merge_hash method




# Driver code

fruit = {name: "pineapple"}
weight = {weight: "1 kg"}
taste = {taste: "good"}


p join_hash(fruit, weight, taste) == {:name=>"pineapple", :weight=>"1 kg", :taste=>"good"} 
```